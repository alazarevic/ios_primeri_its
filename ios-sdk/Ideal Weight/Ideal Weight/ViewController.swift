//
//  ViewController.swift
//  Ideal Weight
//
//  Created by Aleksandra Lazarevic on 20.11.21..
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tfName: UITextField!
    @IBOutlet weak var sliderHeight: UISlider!
    @IBOutlet weak var lblHeightValue: UILabel!
    @IBOutlet weak var switchMale: UISwitch!
    @IBOutlet weak var switchFemale: UISwitch!
    @IBOutlet weak var btnCalculate: UIButton!
    @IBOutlet weak var tvResult: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        sliderHeight.value = 175
        lblHeightValue.text = "\(Int(sliderHeight.value))"
        switchFemale.isOn = false
        switchMale.isOn = true
        checkGender()
    }
    var gender = ""
    
    func calculate() {
        let height = Int(lblHeightValue.text!)
        var idealWeight = 0
        
        switch gender {
        case "m":
            idealWeight = height! - 100
        case "f":
            idealWeight = height! - 110
        default:
            break
        }
        tvResult.text = "Hi \(tfName.text ?? ""), your ideal weight is \(idealWeight) kg!"
    }
    
    
    @IBAction func switchMaleValueChanged(_ sender: Any) {
        if switchMale.isOn {
            switchFemale.setOn(false, animated: true)
        }
        else {
            switchFemale.setOn(true, animated: true)
        }
        checkGender()
    }
    
    
    @IBAction func switchFemaleValueChanged(_ sender: Any) {
        if switchFemale.isOn {
            switchMale.setOn(false, animated: true)
        }
        else {
            switchMale.setOn(true, animated: true)
        }
        checkGender()
        
    }
    
    func checkGender () {
        if switchMale.isOn {
            gender = "m"
        }
        else {
            gender = "f"
        }
    }
    
    @IBAction func sliderHeightValueChanged(_ sender: Any) {
        lblHeightValue.text = "\(Int(sliderHeight.value))"
    }
    
    @IBAction func btnCalculateTapped(_ sender: Any) {
        calculate()
    }
}

