//
//  ViewController.swift
//  Kontrole
//
//  Created by Aleksandra Lazarevic on 14.11.21..
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var lblYourChoice: UILabel!
    @IBOutlet weak var lblComputerChoice: UILabel!
    
    @IBOutlet weak var btnPaper: UIButton!
    @IBOutlet weak var btnRock: UIButton!
    @IBOutlet weak var btnScissors: UIButton!
    
    
    @IBOutlet weak var lblYourScore: UILabel!
    @IBOutlet weak var lblComputerScore: UILabel!
    
    @IBOutlet weak var lblResultText: UILabel!
    
    var status: GameStatus?
    
    var yourPoints:Int = 0
    var computerPoints:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

    }

    enum Choices: String {
        case paper = "✋🏻"
        case rock = "👊🏻"
        case scissors = "✌🏻"
    }
    
    enum GameStatus {
            case yourVictory
            case computersVictory
            case even
        }

    
    @IBAction func btnPaperTapped(_ sender: Any) {
        self.lblYourChoice.text = Choices.paper.rawValue
        computerMakesChoice()
    }
    
    @IBAction func btnRockTapped(_ sender: Any) {
        self.lblYourChoice.text = Choices.rock.rawValue
        computerMakesChoice()
    }
    
    @IBAction func btnScissorsTapped(_ sender: Any) {
        self.lblYourChoice.text = Choices.scissors.rawValue
        computerMakesChoice()
    }
    
    func computerMakesChoice() {
        let choices = [Choices.paper, Choices.rock, Choices.scissors]
        
        let rndNumber = Int(arc4random_uniform(UInt32(3)))
        
        let choice = choices[rndNumber]
        
        lblComputerChoice.text = choice.rawValue
        
        checkGameStatus()
    }
    
    func checkGameStatus() {
        if lblYourChoice.text ==  lblComputerChoice.text {
            status = .even
        }
       else if lblYourChoice.text == Choices.paper.rawValue && lblComputerChoice.text == Choices.rock.rawValue {
            status = .yourVictory
      }
      else if lblYourChoice.text == Choices.scissors.rawValue && lblComputerChoice.text == Choices.paper.rawValue {
              status = .yourVictory
          }
       else if lblYourChoice.text == Choices.rock.rawValue && lblComputerChoice.text == Choices.scissors.rawValue {
                    status = GameStatus.yourVictory
                }
       else {
        status = .computersVictory
       }
        
        switch status {
               case .yourVictory:
                lblResultText.text = "You WIN!"
                   view.backgroundColor = .green
                   yourPoints += 1
                   lblYourScore.text = "\(yourPoints)"

               case .computersVictory:
                lblResultText.text = "You LOSE!"
                   view.backgroundColor = .red
                   computerPoints += 1
                   lblComputerScore.text = "\(computerPoints)"

               case .even:
                lblResultText.text = "Play again..."
                   view.backgroundColor = .lightText
        default:
            print("Wrong state")
        
    }
}
}
