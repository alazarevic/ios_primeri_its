//
//  itp_08_ex04.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/31/21.
//

import Foundation

func bankomat(stanje: Double, iznos: Double) -> String {
    if stanje >= iznos {
        return "Uspešna transakcija. Trenutno stanje je: \(stanje - iznos)"
    }
    else {
        return "Greška. Nemate dovoljno sredstava na računu za ovu transakciju."
    }
}

let poruka = bankomat(stanje: 100, iznos: 50)
print(poruka)
