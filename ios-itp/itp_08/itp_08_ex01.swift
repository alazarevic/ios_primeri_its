//
//  itp_08_ex01.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/31/21.
//

import Foundation

func myFunction(name: String){
    print("Hello, \(name)!")
}

myFunction(name:"John")
