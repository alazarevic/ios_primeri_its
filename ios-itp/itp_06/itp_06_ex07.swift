//
//  itp_06_ex07.swift
//  
//
//  Created by Aleksandra Lazarevic on 4/5/21.
//

import Foundation

var carX: Int = 0
var destinationX: Int = 10
var carSpd: Int = 2

carX += carSpd
print("Dest: ", carX >= destinationX)
carX += carSpd
print("Dest: ", carX >= destinationX)
carX += carSpd
print("Dest: ", carX >= destinationX)
carX += carSpd
print("Dest: ", carX >= destinationX)
carX += carSpd
print("Dest: ", carX >= destinationX)

