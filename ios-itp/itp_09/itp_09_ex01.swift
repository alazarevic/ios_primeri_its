//
//  itp_09_ex01.swift
//  
//
//  Created by Aleksandra Lazarevic on 4/2/21.
//

import Foundation

var startYear = 2010
var endYear = 2020

print("********* Years ***********")

for year in startYear...endYear{
    print(year)
}

print("***************************")
