//
//  itp_09_ex02.swift
//  
//
//  Created by Aleksandra Lazarevic on 4/2/21.
//

import Foundation

let number = 5

print("1\t2\t3")
print("******************")

for num in 1...number{
    print(num*1, separator: "", terminator: "\t")
    print(num*2, separator: "", terminator: "\t")
    print(num*3, separator: "", terminator: "\t")
    print("")
}
