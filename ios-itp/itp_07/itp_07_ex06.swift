//
//  itp_06_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var direction = "N"

switch direction{
    case "N":
        print("North")
    case "E":
        print("East")
    case "S":
        print("South")
    case "W":
        print("West")
default:
    print("Invalid value.")
}
