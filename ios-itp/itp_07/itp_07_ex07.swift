//
//  itp_06_ex07.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var a: Int = 10
var b: Int = 5

var operation: String = "+"

var result: Int = 0

switch operation {

    case "+":
        result = a + b
        print("Result is: \(result).")

case "-":
        result = a - b
        print("Result is: \(result).")

case "*":
        result = a * b
        print("Result is: \(result).")

case "/":
        result = a / b
        print("Result is: \(result).")
    
default:
        print("You did not select the appropriate operator")

}
