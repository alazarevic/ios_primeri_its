//
//  itp_06_ex05.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var age: Int = 15
if age >= 13 {
    print("Access allowed")
}
if age < 13 {
    print("Access denied")
}
