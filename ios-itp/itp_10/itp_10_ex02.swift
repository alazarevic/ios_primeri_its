var parni = [Int]()
var neparni = [Int]()
var brojevi = [1,5,10,7,2,6,4]

for broj in brojevi {
    if broj % 2 == 0 {
        parni.append(broj)
    }
    else {
        neparni.append(broj)
    }
}
print("Parni: ", parni)
print("Neparni: ", neparni)
