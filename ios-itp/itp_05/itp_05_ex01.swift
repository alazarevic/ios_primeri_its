//
//  itp_05_ex01.swift
//  proba
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation
var model: String = "iPhone 11"
var proizvodjac: String = "Apple"
var slika: String = "...putanja do slike..."
var cena: Float = 650.0
var godinaProizvodnje: Int = 2019

print("Model: ", model)
print("Proizvođač: ", proizvodjac)
print("Cena: ", cena)
print("Godina: ", godinaProizvodnje)
