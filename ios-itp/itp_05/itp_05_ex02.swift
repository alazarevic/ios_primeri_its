//
//  itp_05_ex02.swift
//  
//
//  Created by Aleksandra Lazarevic on 3/30/21.
//

import Foundation

var grad: String = "Beograd"
var temperatura: Float = 20.1
var padavine: Bool = false

print("Grad: ", grad)
print("Temperatura: ", temperatura)
print("Padavine: ", padavine)
